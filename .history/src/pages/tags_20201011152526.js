import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const TagsPage = () => (
  <Layout pageTitle="">
    <SEO title="Tags" />
    <h1>Tags</h1>
  </Layout>
)

export default TagsPage
