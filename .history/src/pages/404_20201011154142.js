import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout pageTitle="Oops, something went wrong...">
    <SEO title="404: Not found" />
    <h1>404: Not Found</h1>
    <Link className="btn btn-primary"></Link>
  </Layout>
)

export default NotFoundPage
