import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const TeamPage = () => (
  <Layout pageTitle="">
    <SEO title="Teams" />
    <h1>Our Team</h1>
  </Layout>
)

export default TeamPage
