import React from 'react';
import { Card, CardTitle, CardBody, Form, FormGroup, Input } from 'reactstrap';

const Sidebar = () => {
    <div>
        <Card>
            <CardBody>
                <CardTitle className="text-center text-uppercase mb-3">
                    Newletter
                </CardTitle>
                <Form className="text-center">
                    <FormGroup>
                        <Input type="email" name="email" placeholder="your email address..." />
                    </FormGroup>
                    <Button className="btn btn-outline-success text-uppercase">
                        Subscribe
                    </Button>
                </Form>
            </CardBody>
        </Card>

        <Card>
            <CardBody>
                <CardTitle className="text-center text-uppercase mb-3">
                    Advertisement
                </CardTitle>
                <img src="https://via.placeholder.com" alt="Advert" style={{ width: '100%' }} />
            </CardBody>
        </Card>
    </div>
}

export default Sidebar