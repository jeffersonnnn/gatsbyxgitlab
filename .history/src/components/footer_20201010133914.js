import React from 'react';

const Footer = () => (
    <div className="site-footer">
        <h4 className="text-center">
            Code Blog
        </h4>
    </div>
)