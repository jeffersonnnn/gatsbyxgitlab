// import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

class Header extends React.Component {}

  constructor(props) {
    super (props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/components/">Components</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Options
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  Option 1
                </DropdownItem>
                <DropdownItem>
                  Option 2
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Reset
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <NavbarText>Simple Text</NavbarText>
        </Collapse>
      </Navbar>
    </div>
    )
  }


  // const [isOpen, setIsOpen] = useState(false);

  // const toggle = () => setIsOpen(!isOpen);

  // return (
  //   <div>
  //     <Navbar color="light" light expand="md">
  //       <NavbarBrand href="/">reactstrap</NavbarBrand>
  //       <NavbarToggler onClick={this.toggle} />
  //       <Collapse isOpen={this.state.isOpen} navbar>
  //         <Nav className="ml-auto" navbar>
  //           <NavItem>
  //             <NavLink href="/components/">Components</NavLink>
  //           </NavItem>
  //           <NavItem>
  //             <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
  //           </NavItem>
  //           <UncontrolledDropdown nav inNavbar>
  //             <DropdownToggle nav caret>
  //               Options
  //             </DropdownToggle>
  //             <DropdownMenu right>
  //               <DropdownItem>
  //                 Option 1
  //               </DropdownItem>
  //               <DropdownItem>
  //                 Option 2
  //               </DropdownItem>
  //               <DropdownItem divider />
  //               <DropdownItem>
  //                 Reset
  //               </DropdownItem>
  //             </DropdownMenu>
  //           </UncontrolledDropdown>
  //         </Nav>
  //         <NavbarText>Simple Text</NavbarText>
  //       </Collapse>
  //     </Navbar>
  //   </div>
  // );


  // <header
  //   style={{
  //     background: `rebeccapurple`,
  //     marginBottom: `1.45rem`,
  //   }}
  // >
  //   <div
  //     style={{
  //       margin: `0 auto`,
  //       maxWidth: 960,
  //       padding: `1.45rem 1.0875rem`,
  //     }}
  //   >
  //     <h1 style={{ margin: 0 }}>
  //       <Link
  //         to="/"
  //         style={{
  //           color: `white`,
  //           textDecoration: `none`,
  //         }}
  //       >
  //         {siteTitle}
  //       </Link>
  //     </h1>
  //   </div>
  // </header>
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
