import React from 'react';

const Footer = () => (
    <div className="site-footer">
        <h4 className="text-center">
            Code Blog
        </h4>
        <p className="text-align">Follow us on social Media</p>
        <div className="footer-social-links">
            <ul className="social-links-list">
                <li>
                    <a href="https://www.facebook.com" className="facebook" rel="noopener noreferrer" target="_blank">
                        </a></li>
            </ul>
        </div>
    </div>
)