import React from "react"
import Layout from "../components/layout"
import Sidebar from "../components/sidebar"
import { graphql } from "gastby"
import SEO from "../components/seo"
import { Badge, Card, CardBody, CardSubtitle, Row, Col }

const SinglePost = ({ data }) => {
  const post = data.markdownRemark.frontmatter
  console.log(post, "getting back into the swing of things")
  return (
    <Layout>
      <SEO title={post.title} />
      <h1>{post.title}</h1>
      <Row>

      </Row>
    </Layout>
  )
}

export const postQuery = graphql`
  query blogPostBySlug($slug: String!) {
    markdownRemark(fields: { path: { eq: $slug } }) {
      id
      html
      frontmatter {
        title
        author
        date(formatString: "MMM Do YYYY")
        tags
        image {
          childImageSharp {
            fluid(maxWidth: 700) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`

export default SinglePost
